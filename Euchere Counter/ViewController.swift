//
//  ViewController.swift
//  Euchere Counter
//
//  Created by Etienne Casassa on 10/24/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var playerOneScore: UILabel!
    
    @IBOutlet weak var playerTwoScore: UILabel!
    
    var initialScorePlayerOne = 0
    var initialScorePlayerTwo = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func resetScores(_ sender: UIButton) {
        initialScorePlayerOne = 0
        initialScorePlayerTwo = 0
        self.loadView()
    }

    @IBAction func playerOneMinus(_ sender: UIButton) {
        initialScorePlayerOne -= 1
        playerOneScore.text = String(initialScorePlayerOne)
    }
    
    @IBAction func playerOnePlus(_ sender: UIButton) {
        initialScorePlayerOne += 1
        playerOneScore.text = String(initialScorePlayerOne)
    }
    
    @IBAction func playerOneMinusTwelve(_ sender: UIButton) {
        initialScorePlayerOne -= 12
        playerOneScore.text = String(initialScorePlayerOne)
    }
    
    @IBAction func playerOnePlusTwelve(_ sender: UIButton) {
        initialScorePlayerOne += 12
        playerOneScore.text = String(initialScorePlayerOne)
    }
    
    @IBAction func playerTwoMinus(_ sender: UIButton) {
        initialScorePlayerTwo -= 1
        playerTwoScore.text = String(initialScorePlayerTwo)
    }
    
    @IBAction func playerTwoPlus(_ sender: UIButton) {
        initialScorePlayerTwo += 1
        playerTwoScore.text = String(initialScorePlayerTwo)
    }
    
    @IBAction func playerTwoMinusTwelve(_ sender: UIButton) {
        initialScorePlayerTwo -= 12
        playerTwoScore.text = String(initialScorePlayerTwo)
    }
    
    @IBAction func playerTwoPlusTwelve(_ sender: UIButton) {
        initialScorePlayerTwo += 12
        playerTwoScore.text = String(initialScorePlayerTwo)
    }
    
    override open var shouldAutorotate: Bool {
       return false
    }

    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
       return .portrait
    }
}
